#!/usr/bin/env bash

echo "Merged branches are:"
echo $(git branch --merged)

echo "Current branch is"
echo $(git branch --show-current)
